# coding: utf8
from django.db import models


class Content(models.Model):
    date_created = models.DateTimeField(u'Дата создания')
    content = models.TextField()

    class Meta:
        verbose_name = 'Content'
        verbose_name_plural = 'Content'
        permissions = (
                      ('can_view', u'просмотр'),
                      ('can_edit', u'редактирование'),
        )

    def __str__(self):
        return self.date_created
