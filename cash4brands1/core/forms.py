from django import forms
from django.forms import ModelForm
from .models import Content


class AdvertForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(AdvertForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Content
        fields = '__all__'
