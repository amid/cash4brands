from django.template.response import TemplateResponse
from django.http import HttpResponse
from bs4 import BeautifulSoup
from django.template import loader, Context, Template



from .forms import AdvertForm

from django.shortcuts import render_to_response
from django.template import RequestContext
import json
  

def main(request):
    if request.user.is_staff:
        t = loader.get_template('base.html')
        c = Context({})
        markup = t.render(c)
        soup = BeautifulSoup(markup, "lxml")

        # for item in soup.findAll(['h1', 'h4', 'h5', 'p', 'a']):

        for item in soup.findAll(['h1', 'h4', 'h5', 'p', 'a', 'span']):
            item['contenteditable'] = 'true'
        for item in soup.findAll(True, {'class': [
            'store_title_midle',
            'store_info',
            'store_recomend']
        }):
            item['contenteditable'] = 'true'
        template = Template(soup)
        context = Context({})
        return HttpResponse(template.render(context))
    else:
        return TemplateResponse(request, 'base.html')




def advert(request):
    if request.method == "POST":
        form = AdvertForm(request.POST)

        message = 'something wrong!'
        if(form.is_valid()):
            print(request.POST['content'])
            message = request.POST['content']

        return HttpResponse(json.dumps({'message': message}))

    return render_to_response('advert.html',
            {'form':AdvertForm()}, RequestContext(request))
