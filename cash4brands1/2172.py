#views.py
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, Http404
from django.template.loader import get_template
from django.template import RequestContext
from django.template import Context
from django.utils import simplejson

from forms import MyForm

def ajax_view(request):
    """
    handles the ajax
    """
    template = 'mytemplate/page.html'

    if request.is_ajax():
		if request.method == 'POST':
		    post = request.POST
		    my_form = MyForm(request.POST)
		    if my_form.is_valid():
		        new = my_form.save(commit=True)
		        response = {
		                            'status':True,
		                        }
		    else:
		        response = {
		                            'status':False
		                        }
		    json = simplejson.dumps(response, ensure_ascii=False)
		    return HttpResponse(json, mimetype="application/json")
		
		context = {'my_form':my_form}
		return render_to_response(template, context, context_instance=RequestContext(request))
	else:
		raise Http404


#urls.py
from django.conf.urls.defaults import *

from views import ajax_view

urlpatterns = patterns('', 
    url(r'^$', ajax_view, name="ajax_view"),
)

#page.html

<script type="text/javascript"  src="{{ MEDIA_URL }}js/lib-common/jquery-1.4.2.min.js"></script>
<script type="text/javascript"  src="{{ MEDIA_URL }}js/lib-common/jquery.form.js"></script>

<form action="" method="post" id="myform">
{{ myform.as_p }}
<input type="submit" name="submit" value="SUBMIT" id="submit" />
</form>

<script type="text/javascript">
$(document).ready(
    function() 
    {
	    $('#myform').ajaxForm({
		        beforeSubmit: function(arr, $form, options) {
                        var field=$('#field').val();
                        if (field!=null) {
                            $('#my_form').ajaxStart( function() {
                                    alert('loading');
                                });
                            return true;
				        }
				        else
				        {
                            alert
                            return false;
				        }
			        },	
		        success: function(response) {                    }
		        });
    }
    );
</script>



