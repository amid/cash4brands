# coding: utf8
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
# from django.conf import settings
from django.conf import settings
from cash4brands1.core.views import main
from django.conf import settings
from cash4brands1.core.views import main, advert
admin.autodiscover()


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', main, name='show_content'),
    url(r'^advert/$', advert, name='show_content'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
